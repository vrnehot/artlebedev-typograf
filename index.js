const method = {
	http: require('http'),
	https: require('https')
};


const HEADER = '<?xml version="1.0" encoding="UTF-8"?><soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><ProcessText xmlns="http://typograf.artlebedev.ru/webservices/">';
const FOOTER = '</ProcessText></soap:Body></soap:Envelope>\n';
const PARSER = /<ProcessTextResult>\s*((.|\n)*?)\s*<\/ProcessTextResult>/m;

function parseResponse(xml){
	let res = PARSER.exec(xml);
	res = (res && res[1]) ? res[1] : '';
	res = res.replace(/&gt;/g, '>');
	res = res.replace(/&lt;/g, '<');
	res = res.replace(/&amp;/g, '&');
	return res;
}

typo.HTML_ENTITIES = 1;
typo.XML_ENTITIES = 2;
typo.NO_ENTITIES = 3;
typo.MIXED_ENTITIES = 4;

function typo(text, opt){
	return new Promise((resolve, reject) => {
		opt = opt || {};
		text = text.replace(/<[^>]+>/g, '');
		text = text.replace(/&/g, '&amp;');
		text = text.replace(/</g, '&lt;');
		text = text.replace(/>/g, '&gt;');

		let postData = HEADER +
		'<text>' + text + '</text>' +
		(opt.entityType ? '<entityType>' + opt.entityType + '</entityType>' : '') +
		(opt.noBr ? '<useBr>0</useBr>' : '<useBr>1</useBr>') +
		(opt.noP ? '<useP>0</useP>' : '<useP>1</useP>') +
		(opt.maxNobr ? '<maxNobr>' + opt.maxNobr + '</maxNobr>' : '') +
		(opt.quotA ? '<quotA>' + opt.quotA + '</quotA>' : '') +
		(opt.quotB ? '<quotB>' + opt.quotB + '</quotB>' : '') +
		FOOTER;

		const postOptions = {
			host: 'typograf.artlebedev.ru',
			port: '80',
			path: '/webservices/typograf.asmx',
			method: 'POST',
			headers: {
				'Content-Type': 'application/xml',
				'Content-Length': Buffer.byteLength(postData)
			}
		};

		let request = method[opt.https ? 'https' : 'http'].request(postOptions, res => {
			res.setEncoding('utf8');
			res.on('data', data => {
				resolve(parseResponse(data));
			});
		})

		request.write(postData);
		request.end();
	});
}

module.exports = typo;
